import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseraccountPage } from './useraccount.page';

describe('UseraccountPage', () => {
  let component: UseraccountPage;
  let fixture: ComponentFixture<UseraccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseraccountPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseraccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
