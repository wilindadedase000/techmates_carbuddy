import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NotificationComponent } from '../notification/notification.component';
import { update_user,pull_data } from '../data-schema';
import { DataService } from '../services/data.service';
import { UserService } from '../services/user.service';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-useraccount',
  templateUrl: './useraccount.component.html',
  styleUrls: ['./useraccount.component.scss'],
})
export class UseraccountComponent implements OnInit {
  upd = new update_user();
  pd = new pull_data;
  tk : any ;
  vd :any={
    usr_firstname:"",
    usr_lastname:"",
    usr_gender:0,
    usr_email:""
  };
  constructor(public usr:UserService,public ds:DataService, public modalController: ModalController, public alertController: AlertController) { }

  close() {
    this.modalController.dismiss();
  }
  
  async notification(v) {
    const modal = await this.modalController.create({
      component: NotificationComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }
  

  ngOnInit() {
    this.pullinfo();
  }
  async pullinfo()
  {
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.vid = '0';
    this.pd.website = "www.gordoncollege.edu.ph";
    this.pd.link = 1;
    this.ds.pullData(this.pd, "pullaccountsinfo").subscribe((res)=>{
      this.vd = res[0].result[0];
      });

  }
  async processUpdateU(e){
    e.preventDefault();

    // for(var i=0; i<e.target.length; i++)
    // {
    //   console.log(e.target[i].value+" "+i);
    // }
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.upd.token = this.tk.token;
    this.upd.user_id = this.tk.user_id;
    this.upd.vid = '0';
    this.upd.website = "www.gordoncollege.edu.ph";
    this.upd.link = 'Vehicle';
    this.upd.u_fname=e.target[0].value;
    this.upd.u_lname=e.target[1].value;
    this.upd.u_gender=e.target[2].value;
    this.upd.u_email=e.target[3].value;
    this.upd.u_password=e.target[4].value;
    this.upd.u_newpassword=e.target[5].value;
    var confirmpass=e.target[6].value;
    if(this.verify())
    {
      if(confirmpass==this.upd.u_newpassword)
      {
      this.ds.pullData(this.upd, "upduser").subscribe((res)=>{
      this.vd = res[0].result;
        if(res[0].status[0].remarks=="success")
        {
          this.mAlert("Success",res[0].status[0].message);
        }
        else
        { 
          this.mAlert("Error",res[0].status[0].message)
        }
      },
      err=>{
        this.mAlert("Error","Cannot connect to server")
      });
      }
      else
      this.mAlert("Invalid","New Password and Confirm password didn't match"); 
    }
    else
      this.mAlert("Invalid","Please complete the form");
    
  }
  verify():Boolean
{
  if(this.upd.u_fname.replace(" ","")==""||this.upd.u_fname==null)
    return false;
  else if(this.upd.u_lname.replace(" ","")==""||this.upd.u_lname==null)
    return false;
  else if(this.upd.u_gender==null)
    return false;
  else if(this.upd.u_email.replace(" ","")==""||this.upd.u_email==null)
    return false;
  else if(this.upd.u_password.replace(" ","")==""||this.upd.u_password==null)
    return false;
return true;
} 

async mAlert(hdr,msg) {
  const alert = await this.alertController.create({
    header: hdr,
    // subHeader: 'Subtitle',
    message: msg,
    buttons: ['OK']
  });

  await alert.present();
}
}
