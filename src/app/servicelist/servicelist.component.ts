import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { UserpopoverComponent } from '../userpopover/userpopover.component';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-servicelist',
  templateUrl: './servicelist.component.html',
  styleUrls: ['./servicelist.component.scss'],
})
export class ServicelistComponent implements OnInit {

  constructor(public popoverController: PopoverController,public modalController: ModalController,private router: Router) { }
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserpopoverComponent,
      event: ev,
      translucent: true,

    });
    return await popover.present();
  }


  async opencategory(){
    this.router.navigate(['/service']);
  }

  ngOnInit() {}
  link()
  {}
  public add()
  {}
}
