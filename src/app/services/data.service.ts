import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  apiLink: string = 'http://eodtacticalsolutionsph.com/tm_car/'; //Server
//  apiLink: string = 'http://localhost/tm_car/'; //localhost

  constructor(public http:HttpClient) { console.log('Data Service is working...') }

  pullData(data, vAPI){
    return this.http.post(this.apiLink+vAPI+'.php', btoa(JSON.stringify(data)));
  }

}
