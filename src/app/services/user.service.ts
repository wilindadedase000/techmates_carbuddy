import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public v_idcode: string;
  public v_accountinfo: Object;

  private isUserLoggedIn: boolean;
  public username: string;
  public forApproval: number;
  public v_index: number;
  public issuedto:string;

  constructor() { 
    this.isUserLoggedIn = false;
  }
  setUserLoggedIn(){
    this.isUserLoggedIn = true;
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  setUserLoggedOut() {
    this.username = ""; 
    this.isUserLoggedIn = false; 
  }
}
