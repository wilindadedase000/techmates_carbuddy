import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from '../services/data.service';
import { Fuel_Expense_Logs ,pull_data} from '../data-schema';

@Component({
  selector: 'app-add-fuel-expense',
  templateUrl: './add-fuel-expense.component.html',
  styleUrls: ['./add-fuel-expense.component.scss'],
})
export class AddFuelExpenseComponent implements OnInit {
  fe = new Fuel_Expense_Logs;
  pd = new pull_data;
  tk : any ;
  cars : any;
  constructor(public modalController: ModalController,private ds: DataService) { }

  ngOnInit() {
    this. pullData();
  }
  async close()
  { 
     this.modalController.dismiss();
  }
  async processAddFuel(e){
    e.preventDefault();
    console.log(e);
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.fe.token = this.tk.token;
    this.fe.vrecord_id = e.target.elements[0].value;
    this.fe.ftype =e.target.elements[1].value;
    this.fe.fquantity =e.target.elements[2].value;
    this.fe.fcost =e.target.elements[4].value;
    this.fe.fstation =e.target.elements[6].value;
    this.fe.fodometer=e.target.elements[8].value;
    this.fe.fnote =e.target.elements[10].value;
    this.fe.f_user_id = this.tk.user_id;
    this.fe.website = "www.gordoncollege.edu.ph";
    this.ds.pullData(this.fe, "AddFuel").subscribe((res)=>{
      this.modalController.dismiss(this.fe);
      console.log(res);
      });
  }

  async pullData(){

    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.vid = "0";
    this.pd.website = "www.gordoncollege.edu.ph";
    // this.pd.link = 'Vehicle';
    this.ds.pullData(this.pd, "pullData").subscribe((res)=>{
      
      this.cars = res[0].result;
      console.log(this.cars);
      });
  }
}
