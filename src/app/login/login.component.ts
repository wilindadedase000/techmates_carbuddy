import { Component, OnInit } from '@angular/core';
import { NavController,MenuController} from '@ionic/angular';
import { SignupComponent } from '../signup/signup.component';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { DataService } from '../services/data.service';
import { App_Login } from '../data-schema';
// import { FuelExpenseComponent } from '../fuel-expense/fuel-expense.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  apl = new App_Login();
  tk: Object;
  vData: Object;
  vMessage: string;
  vSuccess: boolean;

  constructor(public menuCtrl:MenuController,public nav: NavController,private router: Router ,private user: UserService, private ds: DataService) { 
  
  // this.user.setUserLoggedOut();
  }

  ngOnInit() {
    this.menuCtrl.enable(false);
    this.chk();
    this.vSuccess = true;
  }
  async register(){
    this.router.navigate(["signup"]);
  }
  processLogin(e){
    e.preventDefault();
    this.apl.m_user = e.target.elements[0].value;
    this.apl.m_password = e.target.elements[2].value;
    // console.log(this.apl.m_user );
    // console.log(this.apl.m_password );
    this.ds.pullData(this.apl, "appLogin").subscribe(
      (res)=>{
        // console.log(res);
        if(res[0].status[0].remarks=="success"){
          this.vData = res[0].result;
          this.user.username=this.apl.m_user;
          this.user.issuedto=this.vData["issued_to"];
          // this.cookieservice.set("__tmcore", JSON.stringify(this.vData));
          localStorage.setItem("__tmcore", JSON.stringify(this.vData));
          this.vSuccess = true;
          this.menuCtrl.enable(true);
          // this.user.setUserLoggedIn();
          this.router.navigate(['carsafety']);
        } else {
          this.vMessage = res[0].status[0].message;
          this.vSuccess = false;
          alert(this.vMessage);
        }
      },
      err => {
        this.vMessage = "Unable to connect to database server. Please contact your web administrator."
        this.vSuccess = false;
        alert(this.vMessage);
      }
    );
  }
  async chk(){
    if(localStorage.getItem("__tmcore")){
      this.menuCtrl.enable(true);
      this.router.navigate(['carsafety']);
    }else{
      
    }
  }
}
