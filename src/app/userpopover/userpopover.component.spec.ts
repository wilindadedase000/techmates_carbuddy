import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserpopoverPage } from './userpopover.page';

describe('UserpopoverPage', () => {
  let component: UserpopoverPage;
  let fixture: ComponentFixture<UserpopoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserpopoverPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserpopoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
