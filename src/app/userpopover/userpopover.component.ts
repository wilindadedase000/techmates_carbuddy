import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { UseraccountComponent } from '../useraccount/useraccount.component';

@Component({
  selector: 'app-userpopover',
  templateUrl: './userpopover.component.html',
  styleUrls: ['./userpopover.component.scss'],
})
export class UserpopoverComponent implements OnInit {

  constructor(public viewCtrl: PopoverController, public modalController: ModalController, public navCtrl: NavController) { }
  close(e) {
    this.viewCtrl.dismiss();
    this.navCtrl.navigateRoot('/login');
  }

  // accountsettings() {
  //   this.viewCtrl.dismiss();
  //   this.navCtrl.navigateRoot('/account-settings');
  // }
  
  async accountsettings(e) {
    this.viewCtrl.dismiss();
    const modal = await this.modalController.create({
      component: UseraccountComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

  ngOnInit() {}
 
}
