import { Component, OnInit } from '@angular/core';
import { ModalController,NavParams  } from '@ionic/angular';
import { pull_data ,Service_History_Logs} from '../data-schema';
import { DataService } from '../services/data.service';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-editservice',
  templateUrl: './editservice.component.html',
  styleUrls: ['./editservice.component.scss'],
})
export class EditserviceComponent implements OnInit {
  sh = new Service_History_Logs;
  id : string;
  pd = new pull_data;
  tk : any ;
  vd :any;
  cars : any;
  base64textString : string;
  constructor(public navParams: NavParams,public modalController: ModalController,private ds: DataService, public alertController: AlertController) { }

  ngOnInit() {
    this.vd=this.navParams.get("value");
    this.pullData();
    this.base64textString=this.vd.simage_data;
    
    }

  close() {
    this.modalController.dismiss();
  }
  public processupdService(e)
  {

    e.preventDefault();
    // console.log(e);
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.sh.token = this.tk.token;
    this.sh.sid=this.vd.sid;
    this.sh.sdate = this.vd.sdate = e.target.elements[0].value;
    this.sh.stype = this.vd.stype = e.target.elements[2].value;
    this.sh.sdue = this.vd.sdue = e.target.elements[4].value;
    this.sh.scost = this.vd.scost = e.target.elements[6].value;
    this.sh.vrecord_id = this.vd.vrecord_id = e.target.elements[8].value;
    this.sh.scenter = this.vd.scenter = e.target.elements[9].value;
    this.sh.snotes = this.vd.snotes = e.target.elements[11].value;
    this.sh.simage_data = this.vd.simage_data = this.base64textString;

    this.sh.s_user_id = this.tk.user_id;

    this.sh.website = "www.gordoncollege.edu.ph";

    if(this.verify())
    {
      this.ds.pullData(this.sh, "updservices").subscribe((res)=>{
        // console.log(res);
        if(res[0].status[0].remarks=="success")
        {
         this.mAlert("Success",res[0].status[0].message)
         this.modalController.dismiss(this.vd);
        }
        else
        { 
          this.mAlert("Error",res[0].status[0].message)
        }
      },
      err=>{
        this.mAlert("Error","Cannot connect to server")
      });
    }
    else
      this.mAlert("Warning","Please complete the form")
    
  }

  verify():Boolean
  {
    if(this.sh.sdate==null)
      return false;
    else if(this.sh.sdue==null)
      return false;
    else if(this.sh.scost==""||this.sh.scost==null)
      return false;
    else if(this.sh.vrecord_id==""||this.sh.vrecord_id==null)
      return false;
    else if(this.sh.scenter==""||this.sh.scenter==null)
      return false;
    return true;
  }
  
  async pullData(){
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.vid = "0";
    this.pd.website = "www.gordoncollege.edu.ph";
    this.pd.link = 1;
    this.ds.pullData(this.pd, "pullData").subscribe((res)=>{
      
      this.cars = res[0].result;
      // console.log(this.cars);
      });
  }
  changeListener($event) : void {
 
    const file = $event.target.files[0];

    if (file) {
      const reader = new FileReader();
  
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      
    }
   
   }
   handleReaderLoaded(e) {
    this.base64textString = 'data:image/png;base64,' + btoa(e.target.result);
    // this.sam.image_data = 'data:image/png;base64,' + btoa(e.target.result);
  }
  async mAlert(hdr,msg) {
    const alert = await this.alertController.create({
      header: hdr,
      // subHeader: 'Subtitle',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

}
