import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditservicePage } from './editservice.page';

describe('EditservicePage', () => {
  let component: EditservicePage;
  let fixture: ComponentFixture<EditservicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditservicePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditservicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
