import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UserpopoverComponent } from './userpopover/userpopover.component';

import { LoginComponent } from './login/login.component';
import { StartComponent } from './start/start.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { ServiceComponent } from './service/service.component';
import { FuelExpenseComponent } from './fuel-expense/fuel-expense.component';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component' ;
import { AddFuelExpenseComponent } from './add-fuel-expense/add-fuel-expense.component';
import { AddServiceComponent } from './add-service/add-service.component';
import { SignupComponent } from './signup/signup.component';
import { VehicleViewComponent } from './vehicle-view/vehicle-view.component';
import { AddReceiptComponent } from './add-receipt/add-receipt.component';
import { FuelViewComponent } from './fuel-view/fuel-view.component';
import { ServiceViewComponent } from './service-view/service-view.component';
import { HomeComponent } from './home/home.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { ServicelistComponent } from './servicelist/servicelist.component';
import { UseraccountComponent } from './useraccount/useraccount.component';
import { NotificationComponent } from './notification/notification.component';
import { EditvehicleComponent } from './editvehicle/editvehicle.component';
import { EditserviceComponent } from './editservice/editservice.component';
import { TravelComponent } from './travel/travel.component';
import { CarsafetyComponent } from './carsafety/carsafety.component';
import { FormsModule } from '@angular/forms';

import { Camera } from '@ionic-native/camera/ngx';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    StartComponent,
    WelcomeComponent,
    VehicleComponent,
    ServiceComponent,
    FuelExpenseComponent,
    AddVehicleComponent,
    AddFuelExpenseComponent,
    AddServiceComponent,
    SignupComponent,
    VehicleViewComponent,
    AddReceiptComponent,
    FuelViewComponent, 
    ServiceViewComponent,
    HomeComponent,
    ExpensesComponent,
    ServicelistComponent,
    UseraccountComponent,
    UserpopoverComponent,

    NotificationComponent,
    EditvehicleComponent,
    EditserviceComponent,
    TravelComponent,
    CarsafetyComponent
 ],
  entryComponents: [
    UserpopoverComponent,
    NotificationComponent,
    EditvehicleComponent,
    EditserviceComponent],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,FormsModule,
    IonicModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
