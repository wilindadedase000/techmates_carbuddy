

export class signup{
  fname : string;
  lname : string;
  gender : number;
  email : string;
  password : string;
}

export class App_Login {
  m_user: string;
  m_password: string;
  m_usr_id : string;
}

 export class Vehicle_Info{
   vid:string;
   vtype : string;
   vmodel : string;
   vyear : string;
   vname : string;
   vnote : string;
   vdata_image : string;
   vcoding : string;


   token : String
   website: string;
   v_user_id : string;
 }
 export class Fuel_Expense_Logs{
   vrecord_id :string;
   ftype : string; 
   fquantity : string;
   fcost : string;
   fstation : string;
   fodometer : string;
   fnote : string;

   f_user_id: string;
   website : string;
   token : string;

 }
 export class Service_History_Logs{
  sid:string;
  vrecord_id : string;
  stype : string ; 
  scenter : string;
  scost : string;
  snotes : string;
  simage_data : string;
  sdate : string;
  sdue: string;
  


  s_user_id: string;
  website : string;
  token : string;
 }

 export class Other_Expenses{
  vrecord_id : string;
  otype : string ; 
  ovendor : string;
  ocost : string;
  onotes : string;
  oodometer : string;

  o_user_id: string;
  website : string;
  token : string;
 }

 export class pull_data{
  user_id: string;
  website : string;
  token : string;
  link : number;
  vid : string;
 }

 export class add_expenses{
  vrecord_id :string;
  exp_purchased : string ;
  exp_item :string;
  exp_quantity :string;
  exp_unit : string;
  exp_total : string;
  exp_center : string;
  exp_note: string ;
  exp_image_data : string;

  exp_user_id: string;
  website : string;
  token : string;
 }


  export class update_user{
  user_id: string;
  website : string;
  token : string;
  link : string;
  vid : string;
  u_fname:string;
  u_lname:string;
  u_gender:string;
  u_email:string;
  u_password:string;
  u_newpassword:string;
 }