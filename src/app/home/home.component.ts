import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { UserpopoverComponent } from '../userpopover/userpopover.component';
import { ModalController } from '@ionic/angular';
import { NotificationComponent } from '../notification/notification.component';
import { DataService } from '../services/data.service';
import { pull_data } from '../data-schema';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  pd = new pull_data;
  tk : any ;
  currentDate=new Date;
  vd :any;
  constructor(public ds:DataService,public popoverController: PopoverController,public modalController: ModalController) { }
    async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserpopoverComponent,
      event: ev,
      translucent: true,
    });
    return await popover.present();
  }

  // async messages() {
  //   const modal = await this.modalController.create({
  //     component: InboxComponent,
  //     componentProps: { value: 123 }
  //   });
  //   return await modal.present();
  // }

  async notification(v) {
    const modal = await this.modalController.create({
      component: NotificationComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

  ngOnInit() {

  this.pullHolidays();
  }
  private pullHolidays()
  {
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.vid = '0';
    this.pd.website = "www.gordoncollege.edu.ph";
    // this.pd.link = 'Vehicle';
    this.ds.pullData(this.pd, "pullholiday").subscribe((res)=>{
      this.vd = res[0].result;
      });
  }
  

}
