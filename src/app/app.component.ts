import { Component } from '@angular/core';

import { Platform,MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { UserpopoverComponent } from './userpopover/userpopover.component';
import { PopoverController } from '@ionic/angular';
import { UserService} from './services/user.service'
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public username : string;
  public u_name:string;
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Vehicle',
      url: '/vehicle',
      icon: 'car'
    },
    {
      title: 'Services',
      url: '/service',
      icon: 'construct'
    },  
    {
      title: 'Travel Safety Checklist',
      url: '/travel',
      icon: 'checkmark-circle-outline'
    }, 
    {
      title: 'Account Settings',
      url: '/useraccount',
      icon: 'settings'
    }
    // ,  
    // {
    //   title: 'Logout',
    //   url: '/login',
    //   icon: 'log-out'
    // }  
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public popoverController: PopoverController,
    private router: Router,
    public usr:UserService,
    public menuCtrl:MenuController
  ) {
    
    this.initializeApp();

    this.username=this.usr.username;
    this.u_name=this.usr.issuedto;

    console.log(this.username+" "+this.u_name);
    // console.log(this.usr['issued_to']);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  async logout(){
    localStorage.clear();
    this.menuCtrl.enable(false);
    this.router.navigate(["login"] );
  }
  // async user(){
  //   this.usr = localStorage.getItem('__tmcore');
  //   console.log('rojin');
  // }

}
