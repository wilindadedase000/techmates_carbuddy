import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddVehicleComponent } from '../add-vehicle/add-vehicle.component';
import { pull_data } from '../data-schema';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { UserpopoverComponent } from '../userpopover/userpopover.component';
import { VehicleViewComponent } from '../vehicle-view/vehicle-view.component';
// import { InboxComponent } from '../inbox/inbox.component';
import { NotificationComponent } from '../notification/notification.component';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss'],
})
export class VehicleComponent implements OnInit {
  pd = new pull_data;
  tk : any ;
  vd :any;
  constructor(public modalController: ModalController,private ds: DataService,private router: Router, public popoverController: PopoverController, public alertController: AlertController) { }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserpopoverComponent,
      event: ev,
      translucent: true,

    });
    return await popover.present();
  }
  
  ngOnInit() {
    this.pullData();
    }
  async add() {
    const modal = await this.modalController.create({
      component: AddVehicleComponent ,
      componentProps: { value: 123 }
    });

    modal.onDidDismiss()
    .then((data) => {
      // console.log(data);
      if(data['data']){
      // this.vd.push(data['data']['Vehicle_Info']); 
      // Here's your selected user!
      this.pullData()
      }else{}
      
  });

    return await modal.present();
  }
  async pullData(){

    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.vid = '0';
    this.pd.website = "www.gordoncollege.edu.ph";
    this.pd.link = 1;
    this.ds.pullData(this.pd, "pullData").subscribe((res)=>{
      this.vd = res[0].result;
      });
  }
  async deleteAlert(id) {
    const alert = await this.alertController.create({
      header: 'Delete Vehicle',
      message: 'Are you sure you want to delete?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          cssClass: 'primary',
          handler: () => {
            this.deleteVehicle(id);
          }
        }
      ]
    });

    await alert.present();
  }

  deleteVehicle(id)
  {
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    
    //ID of the Vehicle
    this.pd.vid = id;
    ////
    this.pd.website = "www.gordoncollege.edu.ph";
    this.pd.link = 1;
    this.ds.pullData(this.pd, "delvehicle").subscribe((res)=>{
      if(res[0].status[0].remarks=="OK")
      {
        this.mAlert("Success",res[0].status[0].message);
        this.pullData();
      }
      else
      { 
        this.mAlert("Error",res[0].status[0].message);
        this.pullData();
      }

    });

      
  }
  async view(id){
    this.router.navigate(["vehicle-view"] , id);
  }

  async openview(e) {
    const modal = await this.modalController.create({
      component: VehicleViewComponent,
      componentProps: {
         value: this.vd[e]
        }
    });

    modal.onDidDismiss()
    .then((data) => {
      // console.log(data);
      if(data['data']){
        this.pullData();
      }
      
  });
    return await modal.present();
  }

  // async messages() {
  //   const modal = await this.modalController.create({
  //     component: InboxComponent,
  //     componentProps: { value: 123 }
  //   });
  //   return await modal.present();
  // }

  async notification() {
    const modal = await this.modalController.create({
      component: NotificationComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }
  async mAlert(hdr,msg) {
    const alert = await this.alertController.create({
      header: hdr,
      // subHeader: 'Subtitle',
      message: msg,
      buttons: ['OK']
    });
  }
}
