import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleViewPage } from './vehicle-view.page';

describe('VehicleViewPage', () => {
  let component: VehicleViewPage;
  let fixture: ComponentFixture<VehicleViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleViewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
