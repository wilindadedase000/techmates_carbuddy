import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { pull_data } from '../data-schema';
import { DataService } from '../services/data.service';
import { ModalController,NavParams } from '@ionic/angular';
import { EditvehicleComponent } from '../editvehicle/editvehicle.component';

@Component({
  selector: 'app-vehicle-view',
  templateUrl: './vehicle-view.component.html',
  styleUrls: ['./vehicle-view.component.scss'],
})
export class VehicleViewComponent implements OnInit {
  id : String;
  pd = new pull_data;
  tk : any ;
  vd :any;
  constructor( public navParams: NavParams,private ds: DataService,private router: Router,public modalController: ModalController) { 
    // this.id = (this.router.getCurrentNavigation().extras).toString(); 
  }

  ngOnInit() {
    this.vd=[{
      vrecord_id: this.navParams.get("value").vrecord_id,
      vimage_data:this.navParams.get("value").vimage_data,
      vmodel:this.navParams.get("value").vmodel,
      vname:this.navParams.get("value").vname,
      vtype:this.navParams.get("value").vtype,
      vyear:this.navParams.get("value").vyear,
      vcoding:this.navParams.get("value").vcoding,
      vnotes:this.navParams.get("value").vnotes,
    }];
  }

  close() {
    this.modalController.dismiss(this.vd);
  }

  async add() {
    const modal = await this.modalController.create({
      component: EditvehicleComponent,
      componentProps: { value: this.vd }
    });

    modal.onDidDismiss()
    .then((data) => {
      if(data['data']){
        this.vd=[{
          vrecord_id: data['data'].vrecord_id,
          vimage_data:data['data'].vdata_image,
          vmodel:data['data'].vmodel,
          vname:data['data'].vname,
          vtype:data['data'].vtype,
          vyear:data['data'].vyear,
          vcoding:data['data'].vcoding,
          vnotes:data['data'].vnote,
        }];
       // Here's your selected user!
      
      }
      
  });
    return await modal.present();
  }

}
