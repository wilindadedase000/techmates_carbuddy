import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { pull_data} from '../data-schema';
import { DataService } from '../services/data.service';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {
  pd = new pull_data;
  tk : any ;
  notification : any ;
  constructor(public modalController: ModalController,private ds: DataService) { }
  
  close() {
    this.modalController.dismiss();
  }



  ngOnInit() {
    this.pullData();
    this.pullNotif();
  }
  async pullData(){
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.vid = "0";
    this.pd.website = "www.gordoncollege.edu.ph";
    this.pd.link = 7;
    this.ds.pullData(this.pd, "pullData").subscribe((res)=>{
      this.notification = (res[0].result);
      
      console.log(res);
      });
  }

  async pullNotif(){
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.vid = "0";
    this.pd.website = "www.gordoncollege.edu.ph";
    this.pd.link = 8;
    this.ds.pullData(this.pd, "pullData").subscribe((res)=>{
      this.notification.push(res[0].result);
      
      console.log(res);
      });
  }
}
