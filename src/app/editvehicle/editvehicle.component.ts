import { Component, OnInit } from '@angular/core';
import { ModalController ,NavParams} from '@ionic/angular';
import { DataService } from '../services/data.service';
import { Vehicle_Info} from '../data-schema';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-editvehicle',
  templateUrl: './editvehicle.component.html',
  styleUrls: ['./editvehicle.component.scss'],
})
export class EditvehicleComponent implements OnInit {

  vi = new Vehicle_Info();
  tk : any;
  vd:any;
  base64textString : string;
  constructor( public navParams: NavParams,public modalController: ModalController,private ds: DataService, public alertController: AlertController) { }

  ngOnInit() {
    this.vd=[{
      vrecord_id: this.navParams.get("value")[0].vrecord_id,
      vimage_data:this.navParams.get("value")[0].vimage_data,
      vmodel:this.navParams.get("value")[0].vmodel,
      vname:this.navParams.get("value")[0].vname,
      vtype:this.navParams.get("value")[0].vtype,
      vyear:this.navParams.get("value")[0].vyear,
      vcoding:this.navParams.get("value")[0].vcoding,
      vnotes:this.navParams.get("value")[0].vnotes,
    }];
    this.base64textString=this.vd[0].vimage_data;
  }
  async close()
  { 
    // console.log("a");
    this.modalController.dismiss();
  }

  async processUpdVehicle(e){
  e.preventDefault();
  // console.log(e);
  // this.apl.m_password = e.target.elements[2].value;
  this.tk = JSON.parse(localStorage.getItem('__tmcore'));
  this.vi.token = this.tk.token;
  
  this.vi.vid=this.vd[0].vrecord_id;
  this.vi.vtype = e.target.elements[0].value;
  this.vi.vname = e.target.elements[1].value;
  this.vi.vmodel = e.target.elements[3].value;
  this.vi.vdata_image = this.base64textString;
  this.vi.vyear = e.target.elements[5].value;

  this.vi.vcoding  =e.target.elements[7].value
  this.vi.vnote = e.target.elements[8].value;
  this.vi.v_user_id = this.tk.user_id;
  this.vi.website = "www.gordoncollege.edu.ph";
  if(this.verify())
  {
    this.ds.pullData(this.vi, "updvehicle").subscribe((res)=>{
      // console.log(res);
      if(res[0].status[0].remarks=="success")
      {
       this.mAlert("Success",res[0].status[0].message)
       this.modalController.dismiss(this.vi);
      }
      else
      { 
        this.mAlert("Error",res[0].status[0].message)
      }
    },
    err=>{
      this.mAlert("Error","Cannot connect to server")
    });
  }
  else
    this.mAlert("Warning","Please complete the form")


  }
  changeListener($event) : void {
 
    const file = $event.target.files[0];

    if (file) {
      const reader = new FileReader();
  
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      
    }
   
   }
   handleReaderLoaded(e) {
    this.base64textString = 'data:image/png;base64,' + btoa(e.target.result);
    // this.sam.image_data = 'data:image/png;base64,' + btoa(e.target.result);
  }
  verify():Boolean
{
  if(this.vi.vtype==""||this.vi.vtype==null)
    return false;
  else if(this.vi.vtype==""||this.vi.vtype==null)
    return false;
  else if(this.vi.vname==""||this.vi.vname==null)
    return false;
  else if(this.vi.vmodel==""||this.vi.vmodel==null)
    return false;
  else if(this.vi.vyear==""||this.vi.vyear==null)
    return false;
  else if(this.vi.vcoding==""||this.vi.vcoding==null)
    return false;
  return true;
  }
  async mAlert(hdr,msg) {
    const alert = await this.alertController.create({
      header: hdr,
      // subHeader: 'Subtitle',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
}
