import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditvehiclePage } from './editvehicle.page';

describe('EditvehiclePage', () => {
  let component: EditvehiclePage;
  let fixture: ComponentFixture<EditvehiclePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditvehiclePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditvehiclePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
