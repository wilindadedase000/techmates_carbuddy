import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddServiceComponent } from '../add-service/add-service.component';
import { pull_data } from '../data-schema';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { ServiceViewComponent } from '../service-view/service-view.component';
import { PopoverController } from '@ionic/angular';
import { UserpopoverComponent } from '../userpopover/userpopover.component';
import { AlertController } from '@ionic/angular';
import { NotificationComponent } from '../notification/notification.component';


@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss'],
})
export class ServiceComponent implements OnInit {
  pd = new pull_data;
  tk : any ;
  sd : any ;
  today: number = Date.now();
  constructor(public modalController: ModalController,private ds: DataService,private router: Router,public popoverController: PopoverController, public alertController: AlertController) { }

  ngOnInit() {
    this.pullData();
    }

  async add() {
    const modal = await this.modalController.create({
      component: AddServiceComponent  ,
      componentProps: { value: 123 }
    });

    modal.onDidDismiss()
    .then((data) => {
      if(data['data']){
      this.pullData(); // Here's your selected user!
      }else{}
      
  });
    return await modal.present();
  }
  async pullData(){
 
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.vid = '0';
    this.pd.user_id = this.tk.user_id;
    this.pd.website = "www.gordoncollege.edu.ph";
    this.pd.link = 5;
    this.ds.pullData(this.pd, "pullData").subscribe((res)=>{
      this.sd= res[0].result;

      });
  }
  async view(id){

    this.router.navigate(["service-view"] , id);
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserpopoverComponent,
      event: ev,
      translucent: true,

    });
    return await popover.present();
  }


  async openview(e) {
    const modal = await this.modalController.create({
      component: ServiceViewComponent,
      componentProps: { 
        value:this.sd[e] 
      }
    });
    modal.onDidDismiss()
    .then((data) => {
      this.pullData(); // Here's your selected user!
    });
    return await modal.present();
  }

  async notification() {
    const modal = await this.modalController.create({
      component: NotificationComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

  async deleteAlert(id) {
    const alert = await this.alertController.create({
      header: 'Delete Service',
      message: 'Are you sure you want to delete?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {
            console.log("Cancel");
          }
        }, {
          text: 'Delete',
          cssClass: 'primary',
          handler: () => {
            this.deleteservices(id);
          }
        }
      ]
    });

    await alert.present();
  }

  deleteservices(id)
  {
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    
    //ID of the Vehicle
    this.pd.vid = id;
    ////
    this.pd.website = "www.gordoncollege.edu.ph";

    this.ds.pullData(this.pd, "delservices").subscribe((res)=>{
      if(res[0].status[0].remarks=="success")
      {
        this.mAlert("Success",res[0].status[0].message);
        this.pullData();
      }
      else
      { 
        this.mAlert("Error",res[0].status[0].message);
      }
      });   
  }
  async mAlert(hdr,msg) {
    const alert = await this.alertController.create({
      header: hdr,
      // subHeader: 'Subtitle',
      message: msg,
      buttons: ['OK']
    });
  }
}
