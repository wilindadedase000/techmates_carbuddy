import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuelViewPage } from './fuel-view.page';

describe('FuelViewPage', () => {
  let component: FuelViewPage;
  let fixture: ComponentFixture<FuelViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuelViewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuelViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
