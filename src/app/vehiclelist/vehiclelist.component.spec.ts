import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclelistPage } from './vehiclelist.page';

describe('VehiclelistPage', () => {
  let component: VehiclelistPage;
  let fixture: ComponentFixture<VehiclelistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiclelistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclelistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
