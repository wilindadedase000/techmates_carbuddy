import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReceiptPage } from './add-receipt.page';

describe('AddReceiptPage', () => {
  let component: AddReceiptPage;
  let fixture: ComponentFixture<AddReceiptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReceiptPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReceiptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
