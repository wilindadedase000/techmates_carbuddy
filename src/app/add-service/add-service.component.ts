import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from '../services/data.service';
import { Service_History_Logs ,pull_data} from '../data-schema';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.scss'],
})
export class AddServiceComponent implements OnInit {
  sh = new Service_History_Logs;
  tk : any ;
  cars : any;
  pd = new pull_data;
  base64textString : string;
  constructor(public modalController: ModalController,private ds: DataService, public alertController: AlertController) { }

  ngOnInit() {
  this.pullData();
  }
  async close()
  { 
    console.log("a");
    this.modalController.dismiss();
  }
  changeListener($event) : void {
 
    const file = $event.target.files[0];

    if (file) {
      const reader = new FileReader();
  
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      
    }
   
   }
   handleReaderLoaded(e) {
    this.base64textString = 'data:image/png;base64,' + btoa(e.target.result);
    // this.sam.image_data = 'data:image/png;base64,' + btoa(e.target.result);
  }
  async processAddService(e){
    e.preventDefault();
    // console.log(e);
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.sh.token = this.tk.token;
    this.sh.sdate = e.target.elements[0].value;
    this.sh.stype = e.target.elements[2].value;
    this.sh.sdue = e.target.elements[4].value;
    this.sh.scost = e.target.elements[6].value;
    this.sh.vrecord_id = e.target.elements[8].value;
    this.sh.scenter = e.target.elements[9].value;
    this.sh.snotes = e.target.elements[11].value;
    this.sh.simage_data = this.base64textString;

    this.sh.s_user_id = this.tk.user_id;
    this.sh.website = "www.gordoncollege.edu.ph";
    if(this.verify())
    {
      this.ds.pullData(this.sh, "AddService").subscribe((res)=>{
        // console.log(res);
        if(res[0].status[0].remarks=="success")
        {
         this.mAlert("Success",res[0].status[0].message)
         this.modalController.dismiss(this.sh);
        }
        else
        { 
          this.mAlert("Error",res[0].status[0].message)
        }
      },
      err=>{
        this.mAlert("Error","Cannot connect to server")
      });
    }
  else
    this.mAlert("Warning","Please complete the form")
  }
  verify():Boolean
  {

    if(this.sh.sdate==null)
      return false;
    else if(this.sh.sdue==null)
      return false;
    else if(this.sh.scost==""||this.sh.scost==null)
      return false;
    else if(this.sh.vrecord_id==""||this.sh.vrecord_id==null)
      return false;
    else if(this.sh.scenter==""||this.sh.scenter==null)
      return false;
    return true;
  }

  async pullData(){
    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.vid = "0";
    this.pd.website = "www.gordoncollege.edu.ph";
    this.pd.link = 1;
    this.ds.pullData(this.pd, "pullData").subscribe((res)=>{
      
      this.cars = res[0].result;
      // console.log(this.cars);
      });
  }

  async mAlert(hdr,msg) {
    const alert = await this.alertController.create({
      header: hdr,
      // subHeader: 'Subtitle',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
}
