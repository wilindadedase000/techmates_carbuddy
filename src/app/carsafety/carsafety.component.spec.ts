import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsafetyPage } from './carsafety.page';

describe('CarsafetyPage', () => {
  let component: CarsafetyPage;
  let fixture: ComponentFixture<CarsafetyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarsafetyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsafetyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
