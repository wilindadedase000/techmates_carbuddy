import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { signup } from '../data-schema';
import { version } from 'punycode';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  us = new signup;
  constructor(private router: Router,private ds: DataService) { }

  ngOnInit() {}
  async login(){
    this.router.navigate(["login"] );
  }
  async signup(e){
  e.preventDefault();
  // console.log(e);
  this.us.fname = e.target.elements[0].value;
  this.us.lname = e.target.elements[2].value;
  this.us.gender = e.target.elements[4].value;
  this.us.email = e.target.elements[5].value;
  this.us.password = e.target.elements[7].value;
  if(this.verify())
  {
    this.ds.pullData(this.us, "register_user").subscribe((res)=>{
      // console.log(res);
      if(res[0].status[0].remarks=="success")
      this.router.navigate(["login"] );
      else
      alert(res[0].status[0].message);
    },
    err => {
      alert("Unable to connect to server.");
    });
  }
  else
  {
    alert("Please complete the form");
  }

}
verify():Boolean
{
  if(this.us.fname.replace(" ","")==""||this.us.fname==null)
    return false;
  else if(this.us.lname.replace(" ","")==""||this.us.lname==null)
    return false;
  else if(this.us.gender==null)
    return false;
  else if(this.us.email.replace(" ","")==""||this.us.email==null)
    return false;
  else if(this.us.password.replace(" ","")==""||this.us.password==null)
    return false;
return true;
}
}
