import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddFuelExpenseComponent } from '../add-fuel-expense/add-fuel-expense.component';
import { FuelViewComponent } from '../fuel-view/fuel-view.component';
import { pull_data } from '../data-schema';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { UserpopoverComponent } from '../userpopover/userpopover.component';
@Component({
  selector: 'app-fuel-expense',
  templateUrl: './fuel-expense.component.html',
  styleUrls: ['./fuel-expense.component.scss'],
})
export class FuelExpenseComponent implements OnInit {
  pd = new pull_data;
  tk : any ;
  fe : any;
  today: number = Date.now();
  constructor(public modalController: ModalController,private ds: DataService,private router: Router,public popoverController: PopoverController) { }

  ngOnInit() {
  this.pullData();
  }
  async add() {
    const modal = await this.modalController.create({
      component: AddFuelExpenseComponent ,
      componentProps: { value: 123 }
    });
 

    modal.onDidDismiss()
    .then((data) => {
      console.log(data);
      if(data['data']){
      this.fe.push(data['data']); // Here's your selected user!
      }else{}
      
  });
  return await modal.present();
  }
  async pullData(){

    this.tk = JSON.parse(localStorage.getItem('__tmcore'));
    this.pd.token = this.tk.token;
    this.pd.user_id = this.tk.user_id;
    this.pd.website = "www.gordoncollege.edu.ph";
    // this.pd.link = 'Fuel_Logs';
    this.pd.vid = '0';
    this.ds.pullData(this.pd, "pullData").subscribe((res)=>{
      
      this.fe = res[0].result;
      console.log(this.fe);
      });
  }
  // async view(id){
  //   this.router.navigate(["fuel-view"] , id);
  // }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserpopoverComponent,
      event: ev,
      translucent: true,

    });
    return await popover.present();
  }

  async openview() {
    const modal = await this.modalController.create({
      component: FuelViewComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }
}
