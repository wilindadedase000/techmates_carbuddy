import { Component, OnInit  } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from '../services/data.service';
import { Vehicle_Info} from '../data-schema';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.scss'],
})
export class AddVehicleComponent implements OnInit {
  vi = new Vehicle_Info();
  tk : any;
  base64textString : string;
  constructor(public modalController: ModalController,private ds: DataService, public alertController: AlertController) { }

  ngOnInit() {}
  async close()
  { 
    // console.log("a");
    this.modalController.dismiss();
  }

  async processAddVehicle(e){
  e.preventDefault();
  // console.log(e);
  // this.apl.m_password = e.target.elements[2].value;
  this.tk = JSON.parse(localStorage.getItem('__tmcore'));
  this.vi.token = this.tk.token;
  // console.log(this.tk.token)
  this.vi.vtype = e.target.elements[0].value;
  this.vi.vname = e.target.elements[1].value;
  this.vi.vmodel = e.target.elements[3].value;
  this.vi.vdata_image = this.base64textString;
  this.vi.vyear = e.target.elements[5].value;

  this.vi.vcoding  =e.target.elements[7].value
  this.vi.vnote = e.target.elements[8].value;
  this.vi.v_user_id = this.tk.user_id;
  this.vi.website = "www.gordoncollege.edu.ph";
  if(this.verify())
  {
    this.ds.pullData(this.vi, "AddVehicle").subscribe((res)=>{
      // console.log(res);
      if(res[0].status[0].remarks=="success")
      {
       this.mAlert("Success",res[0].status[0].message)
       this.modalController.dismiss(this.vi);
      }
      else
      { 
        this.mAlert("Error",res[0].status[0].message)
      }
    },
    err=>{
      this.mAlert("Error","Cannot connect to server")
    });
  }
  else
    this.mAlert("Warning","Please complete the form")
  


  }
verify():Boolean
{
  if(this.vi.vtype==""||this.vi.vtype==null)
    return false;
  else if(this.vi.vtype==""||this.vi.vtype==null)
    return false;
  else if(this.vi.vname==""||this.vi.vname==null)
    return false;
  else if(this.vi.vmodel==""||this.vi.vmodel==null)
    return false;
  else if(this.vi.vyear==""||this.vi.vyear==null)
    return false;
  else if(this.vi.vcoding==""||this.vi.vcoding==null)
    return false;
  return true;
  }
  async mAlert(hdr,msg) {
    const alert = await this.alertController.create({
      header: hdr,
      // subHeader: 'Subtitle',
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }
  changeListener($event) : void {
 
    const file = $event.target.files[0];

    if (file) {
      const reader = new FileReader();
  
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      
    }
   
   }
   handleReaderLoaded(e) {
    this.base64textString = 'data:image/png;base64,' + btoa(e.target.result);
    // this.sam.image_data = 'data:image/png;base64,' + btoa(e.target.result);
  }

}