import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './start/start.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { ServiceComponent } from './service/service.component';
import { FuelExpenseComponent } from './fuel-expense/fuel-expense.component' ;
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component' ;
import { AddFuelExpenseComponent } from './add-fuel-expense/add-fuel-expense.component' ;
import { AddServiceComponent } from './add-service/add-service.component';
import { LoginComponent  } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { VehicleViewComponent } from './vehicle-view/vehicle-view.component';
import { AddReceiptComponent } from './add-receipt/add-receipt.component';
import { FuelViewComponent } from './fuel-view/fuel-view.component';
import { ServiceViewComponent } from './service-view/service-view.component';
import { HomeComponent } from './home/home.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { ServicelistComponent } from './servicelist/servicelist.component';
import { UseraccountComponent } from './useraccount/useraccount.component';
import { TravelComponent } from './travel/travel.component';
import { CarsafetyComponent } from './carsafety/carsafety.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent  },

  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'login',
    component: LoginComponent 
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'start',
    component: StartComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'vehicle',
    component: VehicleComponent
  },
  {
    path: 'vehicle-view',
    component: VehicleViewComponent
  },
  {
    path: 'add-vehicle',
    component: AddVehicleComponent
  },
  {
    path: 'expenses',
    component: ExpensesComponent
  },
  {
    path: 'fuelexpense',
    component: FuelExpenseComponent
  },
  {
    path: 'addfuelexpense',
    component: AddFuelExpenseComponent
  },
  {
    path : 'fuel-view',
    component :  FuelViewComponent
  },
  {
    path: 'servicelist',
    component: ServicelistComponent
  },
  {
    path: 'service',
    component: ServiceComponent
  },
  {
    path : 'service-view',
    component :  ServiceViewComponent
  },
  {
    path: 'addservice',
    component: AddServiceComponent
  },
  {
    path : 'receipt',
    component : AddReceiptComponent
  },
  {
    path : 'useraccount',
    component : UseraccountComponent
  },
  {
    path : 'travel',
    component : TravelComponent
  },
  {
    path : 'carsafety',
    component : CarsafetyComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
