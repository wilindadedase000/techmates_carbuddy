import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { pull_data } from '../data-schema';
import { DataService } from '../services/data.service';
import { ModalController,NavParams } from '@ionic/angular';
import { EditserviceComponent } from '../editservice/editservice.component';

@Component({
  selector: 'app-service-view',
  templateUrl: './service-view.component.html',
  styleUrls: ['./service-view.component.scss'],
})
export class ServiceViewComponent implements OnInit {

  id : String;
  pd = new pull_data;
  tk : any ;
  vd :any;
  constructor(public navParams: NavParams,private ds: DataService,private router: Router, public modalController: ModalController) { 
    // this.id = (this.router.getCurrentNavigation().extras).toString(); 
    // console.log(this.id);
  }
  ngOnInit() {
    this.vd={
      sid:this.navParams.get("value").srecord_id,
      urecord_id: this.navParams.get("value").urecord_id,
      simage_data:this.navParams.get("value").simage_data,
      stype:this.navParams.get("value").stype,
      sdue:this.navParams.get("value").sdue,
      sdate:this.navParams.get("value").sdate,
      scost:this.navParams.get("value").scost,
      vid:this.navParams.get("value").vrecord_id,
      vtype:this.navParams.get("value").vtype,
      vname:this.navParams.get("value").vname,
      scenter:this.navParams.get("value").scenter,
      snotes:this.navParams.get("value").snotes,
    };


  }

  close() {
    this.modalController.dismiss();
  }

  async add() {
    const modal = await this.modalController.create({
      component: EditserviceComponent  ,
      componentProps: { value: this.vd }
    });

    modal.onDidDismiss()
    .then((data) => {
      if(data['data']){
      this.vd=data['data'];
     }

  });
    return await modal.present();
  }
}
