import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceViewPage } from './service-view.page';

describe('ServiceViewPage', () => {
  let component: ServiceViewPage;
  let fixture: ComponentFixture<ServiceViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceViewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
